import type { PatternColumn, PatternGridElement, PatternRow } from '@/types/pattern.types'
import { BulletColor, type BulletNumber } from '../types/bullet'

export const getColStart = (color: BulletColor): string => {
  switch (color) {
    case BulletColor.RED:
      return 'col-start-1'
    case BulletColor.BLUE:
      return 'col-start-2'
    case BulletColor.GREEN:
      return 'col-start-3'
    case BulletColor.YELLOW:
      return 'col-start-4'
    case BulletColor.PURPLE:
      return 'col-start-5'
  }
}

export const getRowStart = (number: number): string => {
  switch (number) {
    case 0:
      return 'row-start-1'
    case 1:
      return 'row-start-2'
    case 2:
      return 'row-start-3'
    case 3:
      return 'row-start-4'
    case 4:
      return 'row-start-5'
    case 5:
      return 'row-start-6'
    case 6:
      return 'row-start-7'
    default:
      return ''
  }
}

export const getBorderColor = (color: BulletColor): string => {
  switch (color) {
    case BulletColor.RED:
      return 'border-red-600 bg-red-600'
    case BulletColor.BLUE:
      return 'border-blue-600 bg-blue-600'
    case BulletColor.GREEN:
      return 'border-green-600 bg-green-600'
    case BulletColor.YELLOW:
      return 'border-yellow-600 bg-yellow-600'
    case BulletColor.PURPLE:
      return 'border-purple-600 bg-purple-600'
  }
}

export const getColorHexValue = (color: BulletColor): string => {
  switch (color) {
    case BulletColor.RED:
      return '#dc2626'
    case BulletColor.BLUE:
      return '#0284c7'
    case BulletColor.GREEN:
      return '#059669'
    case BulletColor.YELLOW:
      return '#d97706'
    case BulletColor.PURPLE:
      return '#7c3aed'
  }
}

export const getFillColor = (color: BulletColor): string => {
  switch (color) {
    case BulletColor.RED:
      return 'fill-red'
    case BulletColor.BLUE:
      return 'fill-blue'
    case BulletColor.GREEN:
      return 'fill-green'
    case BulletColor.YELLOW:
      return 'fill-yellow'
    case BulletColor.PURPLE:
      return 'fill-purple'
  }
}

export const getShadowColor = (color: BulletColor): string => {
  switch (color) {
    case BulletColor.RED:
      return 'shadow-red'
    case BulletColor.BLUE:
      return 'shadow-blue'
    case BulletColor.GREEN:
      return 'shadow-green'
    case BulletColor.YELLOW:
      return 'shadow-yellow'
    case BulletColor.PURPLE:
      return 'shadow-purple'
  }
}

export const getDropShadow = (color: BulletColor): string => {
  switch (color) {
    case BulletColor.RED:
      return 'drop-shadow-red fill-red'
    case BulletColor.BLUE:
      return 'drop-shadow-blue fill-blue'
    case BulletColor.GREEN:
      return 'drop-shadow-green fill-green'
    case BulletColor.YELLOW:
      return 'drop-shadow-yellow fill-yellow'
    case BulletColor.PURPLE:
      return 'drop-shadow-purple fill-purple'
  }
}

export const getDropShadowSm = (color: BulletColor): string => {
  switch (color) {
    case BulletColor.RED:
      return 'drop-shadow-red-sm fill-white'
    case BulletColor.BLUE:
      return 'drop-shadow-blue-sm fill-white'
    case BulletColor.GREEN:
      return 'drop-shadow-green-sm fill-white'
    case BulletColor.YELLOW:
      return 'drop-shadow-yellow-sm fill-white'
    case BulletColor.PURPLE:
      return 'drop-shadow-purple-sm fill-white'
  }
}

export const getColSpan = (number: PatternColumn): string => {
  switch (number) {
    case 1:
      return 'col-span-1'
    case 2:
      return 'col-span-2'
    case 3:
      return 'col-span-3'
    case 4:
      return 'col-span-4'
    case 5:
      return 'col-span-5'
  }
}

export const getRowSpan = (number: PatternRow): string => {
  switch (number) {
    case 1:
      return 'row-span-1'
    case 2:
      return 'row-span-2'
    case 3:
      return 'row-span-3'
    case 4:
      return 'row-span-4'
    case 5:
      return 'row-span-5'
  }
}
