import { Heroine } from './heroine.types'

export type Action = SimpleAction | StarAction

export enum ActionName {
  MOVE_LEFT_RIGHT_DOWN_ONE,
  DRAW_PATTERN,
  MOVE_UP_ONE,
  MOVE_DOWN_ANY,
  GAIN_ONE_AP
}

export type GenericAction = {
  name: ActionName
  description: string
}

export type SimpleAction = GenericAction & {
  apCost: number
}
export type StarAction = GenericAction & {
  starAction: true
  callback: () => void
}

export const esfirActions: Action[] = [
  {
    apCost: 1,
    name: ActionName.MOVE_LEFT_RIGHT_DOWN_ONE,
    description: "Déplacez un projectile d'1 case vers la gauche, la droite ou le bas."
  },
  {
    apCost: 2,
    name: ActionName.DRAW_PATTERN,
    description: 'Piochez un motif.'
  },
  {
    apCost: 2,
    name: ActionName.MOVE_UP_ONE,
    description: "Déplacez un projectile d'1 case vers le haut."
  },
  {
    apCost: 2,
    name: ActionName.MOVE_DOWN_ANY,
    description: "Déplacez un projectile de n'importe quel nombre de cases vers le bas."
  },
  {
    starAction: true,
    name: ActionName.GAIN_ONE_AP,
    description: 'Gagnez 1 PA.',
    callback: () => {}
  }
]
