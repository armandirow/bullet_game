import { esfirActions } from './action.types'
import { esfirPatterns } from './pattern.types'

export enum Heroine {
  ESFIR_VOLKOVA = 'ESFIR VOLKOVA'
  // ALDELHEID_BECKENBAUER = 'ALDELHEID BECKENBAUER',
  // EKOLU_KAPAKAHI = 'EKOLU KAPAKAHI'
}

export const heroineActionsMap = { [Heroine.ESFIR_VOLKOVA]: esfirActions }
export const heroinePatternsMap = { [Heroine.ESFIR_VOLKOVA]: esfirPatterns }
