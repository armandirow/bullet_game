import { BulletColor, type BulletNumber } from './bullet'

export type PatternColumn = 1 | 2 | 3 | 4 | 5
export type PatternRow = 1 | 2 | 3 | 4 | 5

export type Pattern = {
  id: PatternId
  name: string
  columnLength: PatternColumn
  rowLength: PatternRow
  grid: PatternGridElement[][]
}

//prettier-ignore
export type PatternGridElement<T extends PatternElementType = PatternElementType> =
  T extends PatternElementType.BLANK ? BlankCase : 
  T extends PatternElementType.NO_BULLET ? NoBulletCase : 
  T extends PatternElementType.ANY_BULLET ? AnyBulletCase : 
  T extends PatternElementType.DESTROY ? DestroyCase : 
  T extends PatternElementType.COLORED_BULLET ? ColoredBulletCase : 
  T extends PatternElementType.NUMBERED_BULLET ? NumberedBulletCase :
  T extends PatternElementType.SAME_NUMBER ? SameNumberCase : 
  { type: T }

export enum PatternElementType {
  BLANK = 'BLANK',
  NO_BULLET = 'NO_BULLET',
  ANY_BULLET = 'ANY_BULLET',
  DESTROY = 'DESTROY',
  COLORED_BULLET = 'COLORED_BULLET',
  NUMBERED_BULLET = 'NUMBERED_BULLET',
  SAME_NUMBER = 'SAME_NUMBER'
}

export type BlankCase = { type: PatternElementType.BLANK }
export type NoBulletCase = { type: PatternElementType.NO_BULLET }
export type AnyBulletCase = {
  type: PatternElementType.ANY_BULLET
  isdestroyed: boolean
}
export type DestroyCase = {
  type: PatternElementType.DESTROY
  isdestroyed: true
}
export type ColoredBulletCase = {
  type: PatternElementType.COLORED_BULLET
  color: BulletColor
}
export type NumberedBulletCase = {
  type: PatternElementType.NUMBERED_BULLET
  number: BulletNumber
}
export type SameNumberCase = {
  type: PatternElementType.SAME_NUMBER
  number: BulletNumber
  isSameNumber: true
}

const BLANK: PatternGridElement<PatternElementType.BLANK> = {
  type: PatternElementType.BLANK
}
const ANY_BULLET: PatternGridElement<PatternElementType.ANY_BULLET> = {
  type: PatternElementType.ANY_BULLET,
  isdestroyed: false
}
const ANY_BULLET_DESTROY: PatternGridElement<PatternElementType.ANY_BULLET> = {
  type: PatternElementType.ANY_BULLET,
  isdestroyed: true
}
const DESTROY: PatternGridElement<PatternElementType.DESTROY> = {
  type: PatternElementType.DESTROY,
  isdestroyed: true
}

const RED_BULLET: PatternGridElement<PatternElementType.COLORED_BULLET> = {
  type: PatternElementType.COLORED_BULLET,
  color: BulletColor.RED
}
const BLUE_BULLET: PatternGridElement<PatternElementType.COLORED_BULLET> = {
  type: PatternElementType.COLORED_BULLET,
  color: BulletColor.BLUE
}
const GREEN_BULLET: PatternGridElement<PatternElementType.COLORED_BULLET> = {
  type: PatternElementType.COLORED_BULLET,
  color: BulletColor.GREEN
}
const YELLOW_BULLET: PatternGridElement<PatternElementType.COLORED_BULLET> = {
  type: PatternElementType.COLORED_BULLET,
  color: BulletColor.YELLOW
}
const PURPLE_BULLET: PatternGridElement<PatternElementType.COLORED_BULLET> = {
  type: PatternElementType.COLORED_BULLET,
  color: BulletColor.PURPLE
}

export enum PatternId {
  ESFIR_PATTER_1,
  ESFIR_PATTER_2,
  ESFIR_PATTER_3
}

//prettier-ignore
export const esfirPatterns: Pattern[] = [
  {
    id: PatternId.ESFIR_PATTER_1,
    name: 'Etoile filante',
    columnLength: 1,
    rowLength: 5,
    grid: [
      [YELLOW_BULLET],
      [ANY_BULLET   ], 
      [DESTROY      ], 
      [DESTROY      ], 
      [DESTROY      ]
    ]
  },
  {
    id: PatternId.ESFIR_PATTER_2,
    name: 'Vitessse de libération',
    columnLength: 5,
    rowLength: 1,
    grid: [[ANY_BULLET, DESTROY, DESTROY, DESTROY, ANY_BULLET]]
  },
  {
    id: PatternId.ESFIR_PATTER_3,
    name: "Fracass'étoile",
    columnLength: 4,
    rowLength: 3,
    grid: [
      [ANY_BULLET, BLANK  , BLANK  , BLANK  ],
      [ANY_BULLET, DESTROY, DESTROY, DESTROY],
      [ANY_BULLET, BLANK  , BLANK  , BLANK  ]
    ]
  }
]
