import type { UUID } from 'crypto'

export enum BulletColor {
  RED = 'red',
  BLUE = 'blue',
  GREEN = 'green',
  YELLOW = 'yellow',
  PURPLE = 'purple'
}

export type BulletNumber = 1 | 2 | 3 | 4

export type Bullet = {
  id: UUID
  color: BulletColor
  number: BulletNumber
  isStared: boolean
  column: BulletColor
  row: number
  top: number
  left: number
  startAnimation: boolean
}

export type Bag = {
  bullets: Bullet[]
}

export type BulletContainer = Bullet | undefined

export enum Line {
  FIRST = 1,
  SECOND = 2,
  THIRD = 3,
  FOURTH = 4,
  FIFTH = 5,
  SIXTH = 6
}

export type BulletColumn = [
  BulletContainer, // 0
  BulletContainer, // 1
  BulletContainer, // 2
  BulletContainer, // 3
  BulletContainer, // 4
  BulletContainer, // 5
  BulletContainer /// 6
]
// prettier-ignore
export type SightBoard = 
{
  [BulletColor.RED]:    BulletColumn,
  [BulletColor.BLUE]:   BulletColumn,
  [BulletColor.GREEN]:  BulletColumn,
  [BulletColor.YELLOW]: BulletColumn,
  [BulletColor.PURPLE]: BulletColumn,
}
