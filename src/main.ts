import './assets/main.css'

import { createApp, type InjectionKey } from 'vue'
import { createPinia } from 'pinia'

import App from './App.vue'
import router from './router'
import { Notyf } from 'notyf'
import 'notyf/notyf.min.css'

const app = createApp(App)

const notyf = new Notyf({
  duration: 3000,
  dismissible: true,
  position: { x: 'right', y: 'top' }
})
export const NOTYF_INJECTION_KEY: InjectionKey<Notyf> = Symbol('Notyf')

app.use(createPinia())
app.provide(NOTYF_INJECTION_KEY, notyf)
app.use(router)

app.mount('#app')
