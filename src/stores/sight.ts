import { defineStore } from 'pinia'
import { computed, onMounted, ref } from 'vue'
import { useCurrentBagStore } from './current-bag'
import {
  BulletColor as Color,
  type Bullet,
  type BulletColumn,
  type SightBoard
} from '@/types/bullet'

export const useSightStore = defineStore('sight', () => {
  const sightBoard = ref<SightBoard>({
    [Color.RED]: [undefined, undefined, undefined, undefined, undefined, undefined, undefined],
    [Color.BLUE]: [undefined, undefined, undefined, undefined, undefined, undefined, undefined],
    [Color.GREEN]: [undefined, undefined, undefined, undefined, undefined, undefined, undefined],
    [Color.YELLOW]: [undefined, undefined, undefined, undefined, undefined, undefined, undefined],
    [Color.PURPLE]: [undefined, undefined, undefined, undefined, undefined, undefined, undefined]
  })
  const currentBagStore = useCurrentBagStore()

  const bulletsOnBoard = computed(() => {
    return Object.values(sightBoard.value).flatMap((column) =>
      column.filter((bullet): bullet is Bullet => bullet !== undefined)
    )
  })

  const getMoveDownPosition = ({
    bulletColumn,
    rowNumber,
    numberOfMoves
  }: {
    bulletColumn: BulletColumn
    rowNumber: number
    numberOfMoves: number
  }): [number, boolean] => {
    console.log(`next  [${rowNumber}] needToMove: ${numberOfMoves}\n${bulletColumn[rowNumber]}`)
    if (numberOfMoves === 0) {
      return [rowNumber, true]
    }
    if (rowNumber + 1 > 6) {
      return [rowNumber, false]
    }
    if (bulletColumn[rowNumber + 1] !== undefined) {
      return getMoveDownPosition({
        bulletColumn,
        numberOfMoves: numberOfMoves,
        rowNumber: rowNumber + 1
      })
    }
    return getMoveDownPosition({
      bulletColumn,
      numberOfMoves: numberOfMoves - 1,
      rowNumber: rowNumber + 1
    })
  }

  const updateOneBullet = (bullet: Bullet, udatedFields: Partial<Bullet>) => {
    sightBoard.value[bullet.column][bullet.row] = undefined
    const updatedBullet = {
      ...bullet,
      ...udatedFields
    }
    sightBoard.value[updatedBullet.column][updatedBullet.row] = updatedBullet
    return updatedBullet
  }

  const pickOne = async () => {
    const bullet = currentBagStore.pickRandomBullet()
    sightBoard.value[bullet.column][bullet.row] = bullet
    await new Promise((resolve) => setTimeout(resolve, 100))
    const bulletColumn = sightBoard.value[bullet.column]
    const [rowNumber, canMove] = getMoveDownPosition({
      bulletColumn,
      numberOfMoves: bullet.number,
      rowNumber: bullet.row
    })
    if (canMove) {
      updateOneBullet(bullet, { top: rowNumber - bullet.row, startAnimation: true })
      await new Promise((resolve) => setTimeout(resolve, 500))
      updateOneBullet(bullet, {
        row: rowNumber,
        top: 0,
        startAnimation: false
      })
    } else {
      throw new Error('cannot move down, lose 1 hp')
    }
  }

  //isPatternComplete(posx, posy)

  onMounted(() => {})

  return { sightBoard, pickOne, bulletsOnBoard }
})
