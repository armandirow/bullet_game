import { Heroine, heroinePatternsMap } from '@/types/heroine.types'
import type { Pattern, PatternId } from '@/types/pattern.types'
import { getShuffledArray } from '@/utils/math'
import { defineStore } from 'pinia'
import { ref } from 'vue'

const HAND_SIZE = 2

export const usePatternStore = defineStore('pattern', () => {
  const patterns = ref<Pattern[]>([])
  const patternsInHand = ref<Pattern[]>([])
  const patternSelected = ref<Pattern>()

  const drawPattern = () => {
    const [patternDrawn, ...shuffledPatterns] = getShuffledArray(patterns.value)
    patterns.value = [...shuffledPatterns]
    patternsInHand.value.push(patternDrawn)
  }

  const selectPattern = (id: PatternId) => {
    const pattern = patternsInHand.value.find((pattern) => pattern.id === id)
    if (!pattern) {
      patternSelected.value = undefined
      throw new Error(`pattern with id ${id} not found in hand`)
    }
    patternSelected.value = pattern
  }

  const initialize = (hero: Heroine) => {
    patterns.value = heroinePatternsMap[hero]
    patternsInHand.value = []
    for (let i = 0; i < HAND_SIZE; i++) {
      drawPattern()
    }
  }

  return { patterns, patternsInHand, patternSelected, initialize, selectPattern }
})
